# README #

This code allows the user to change a file containing quote inputs (user details, vehicle details, location, etc.) 
and a specified pause page. The code then works through the quoter until the specified pause page, at which time
the user can take over the quote process manually.

### What is this repository for? ###

Saving time when testing the quoter

### How do I get set up? ###

Install python3 on computer
pip3 install selenium
copy chromedriver into path folder
run quote_looper.py from terminal by typing >>python3 quote_looper.py

### Contribution guidelines ###

### Who do I talk to? ###

Chris Cherry
chris@naked.insure